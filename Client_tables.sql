
CREATE TABLE client(
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	client_name VARCHAR(100) NOT NULL
);

CREATE TABLE client_manager_stuff_location(
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
    client_location INTEGER REFERENCES client(id),
    manager_location INTEGER REFERENCES manager(id),
    stuff_location INTEGER REFERENCES stuff(id),
    location VARCHAR(1000)
);

CREATE TABLE manager(
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	manager_name VARCHAR(200)
);

CREATE TABLE contract (
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	client_id INTEGER REFERENCES client(id),
    manager_id INTEGER REFERENCES manager(id),
	estimated_cost FLOAT,
    completion_date Date
);

CREATE TABLE stuff(
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	manager_id INTEGER REFERENCES manager(id),
    contract_id INTEGER REFERENCES contract(id),
	stuff_name VARCHAR(200)
);
